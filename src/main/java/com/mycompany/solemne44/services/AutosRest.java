/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.solemne44.services;

import com.mycompany.solemne44.dao.AutosJpaController;
import com.mycompany.solemne44.dao.exceptions.NonexistentEntityException;
import com.mycompany.solemne44.entity.Autos;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.media.Media;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author nayel
 */
@Path("autos")
public class AutosRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public Response listarAutos() {
        AutosJpaController daoAutos = new AutosJpaController();
        List<Autos> listaautos = daoAutos.findAutosEntities(); //se llena la tabla con los datos
        return Response.ok(200).entity(listaautos).build();// devuelve los datos

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearAutos(Autos datosAutos) {
        AutosJpaController daoAutos = new AutosJpaController();
        try {
            daoAutos.create(datosAutos);
        } catch (Exception ex) {
            Logger.getLogger(AutosRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(datosAutos).build();// devuelve los datos

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)

    public Response editarAutos(Autos datosAutos) {
        AutosJpaController daoAutos = new AutosJpaController();
        try {
            daoAutos.edit(datosAutos);
        } catch (Exception ex) {
            Logger.getLogger(AutosRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(datosAutos).build();// devuelve los datos

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{patente}")//recive la patente
    public Response buscarAutos(@PathParam("patente") int patente)//recive patente tipo entero  
    {
        AutosJpaController daoAutos = new AutosJpaController();
        Autos datosAutos = daoAutos.findAutos(patente);//busca los datos
        return Response.ok(200).entity(datosAutos).build();// devuelve los datos

    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{patente}")//recive la patente
    public Response eliminarAutos(@PathParam("patente") int patente)//recive patente tipo entero  
    {
        AutosJpaController daoAutos = new AutosJpaController();
        try {
            daoAutos.destroy(patente);//elimina busca los datos
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(AutosRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity("{\"Atencion \":\"registro eliminado \"}").build();
    }

}
